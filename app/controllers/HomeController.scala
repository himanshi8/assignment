package controllers

import controllers.HomeController._
import javax.inject._
import play.api.libs.json._
import play.api.mvc._
import service.DeliveryService.Vehicle
import service.OrderService
import service.OrderService.{Order, SlotTimings, VehicleCarryingOrder}

import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(orderService: OrderService) extends Controller {

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def assignOrderToVehicle() = Action.async { implicit request =>
    request.body.asJson match {
      case Some(json) => json.validate[OrderDeliverRequest] match {
        case JsSuccess(value, _) =>
          val orders = value.orders.map { order => Order(order.order_id, order.order_weight, isAssigned = false) }
          orderService.assignOrderToVehicle(value.slot, orders).map { case (vehicles, incompleteOrders) =>
            val deliveredOrders = vehicles.map(HomeController.toApiResult)
            val res = if (incompleteOrders.nonEmpty) {
              Json.obj(nonDeliveredOrders -> incompleteOrders.map { order => Json.obj("order_id" -> order.orderId, "weight" -> order.weight) }, deliveredOrder -> deliveredOrders)
            } else {
              Json.obj(deliveredOrder -> deliveredOrders)
            }
            Ok(res)
          }.recover { case s => BadRequest(Json.obj(status -> failureStatus, reason -> s.getMessage)) }
        case JsError(_) => Future.successful(BadRequest(Json.obj(status -> failureStatus, reason -> s"Incorrect Json")))
      }
      case None => Future.successful(BadRequest(Json.obj(status -> failureStatus, reason -> "Request Data not found.")))
    }
  }
}

object HomeController {

  val reason = "reason"
  val status = "status"
  val deliveredOrder = "delivered_orders"
  val nonDeliveredOrders = "non_delivered_orders"
  val failureStatus = "failed"

  case class VehicleApiResult(vehicle_type: Vehicle, delivery_partner_id: Long, list_order_ids_assigned: List[Long])

  object VehicleApiResult {
    implicit val format: OFormat[VehicleApiResult] = Json.format[VehicleApiResult]
  }

  case class OrderRequest(order_id: Long, order_weight: Double)

  object OrderRequest {
    implicit val reads: Reads[OrderRequest] = Json.reads[OrderRequest]
  }

  case class OrderDeliverRequest(slot: SlotTimings, orders: List[OrderRequest])

  object OrderDeliverRequest {
    implicit val reads: Reads[OrderDeliverRequest] = Json.reads[OrderDeliverRequest]
  }

  def toApiResult(vco: VehicleCarryingOrder) = VehicleApiResult(vco.vehicle, vco.deliveryPartnerId, vco.orderIds.map(_.orderId).toList)
}