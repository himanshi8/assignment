package models

import models.MyPostgresProfile.api._
import service.DeliveryService.Vehicle
import slick.lifted.Tag
import models.MyPostgresProfile.api._
import org.joda.time.DateTime
import slick.lifted.Tag


case class DeliveryPartnerRecord(partnerId: Long, vehicle: String)


class DeliveryPartnerTable(tag: Tag) extends Table[DeliveryPartnerRecord](tag, "delivery_partner_record") {
  def partnerId = column[Long]("partner_id", O.PrimaryKey)

  def partnerName = column[String] ("name")

  def emailAddress = column[String]("email_address")

  def vehicleType = column[String]("vehicle")

  def * = (partnerId, vehicleType) <> (DeliveryPartnerRecord.tupled, DeliveryPartnerRecord.unapply)
}



