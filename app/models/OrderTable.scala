package models

import models.MyPostgresProfile.api._
import org.joda.time.DateTime
import service.OrderService.Order
import slick.lifted.Tag
import scala.concurrent.ExecutionContext.Implicits.global
import scala.collection.mutable.ListBuffer

case class OrderRecord(id: Long, orderId: Long, weight: Double, status: String, addedOn: DateTime, deliverPartnerId: Long)

class OrderTable(tag: Tag) extends Table[OrderRecord](tag, "orders") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def orderId = column[Long]("order_id")

  def weight = column[Double]("weight")

  def status = column[String]("status")

  def addedOn = column[DateTime]("added_on")
  def deliveryPartnerId = column[Long]("partner_id")

  def * = (id, orderId, weight, status, addedOn, deliveryPartnerId) <> (OrderRecord.tupled, OrderRecord.unapply)
  }
object OrderTable {
  val query = TableQuery[OrderTable]

  def create(orders: List[Order]) = {
    DBIO.sequence(orders.map { order =>
      for {
        _ <- query += OrderRecord(-1l, order.orderId, order.weight, "", DateTime.now(), 1)
      }yield{}
    })
  }
}