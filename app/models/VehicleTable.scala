package models

import slick.lifted.Tag
import models.MyPostgresProfile.api._
import service.DeliveryService.Vehicle
import service.OrderService.SlotTimings
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

case class VehicleRecord(id: Long, vehicleId: Long, vehicleType: Vehicle, slotAvailableIn: String)

class VehicleTable(tag: Tag) extends Table[VehicleRecord](tag, "vehicle") {

  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

  def vehicleId = column[Long]("vehicle_id")

  def vehicleType = column[Vehicle]("vehicle_type")

  def slotAvailableIn = column[String]("slot_available")

  def * = (id, vehicleId, vehicleType, slotAvailableIn) <> (VehicleRecord.tupled, VehicleRecord.unapply _)
}

object VehicleTable {
  val query = TableQuery[VehicleTable]

  def getAvailableVehicles(slot: SlotTimings): DBIO[List[Vehicle]] = {
    for {
      vehicles <- VehicleTable.query.filter(_.slotAvailableIn === slot.asString).map(_.vehicleType).result
    } yield vehicles.toList
  }
}