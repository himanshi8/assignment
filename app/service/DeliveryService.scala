package service

import play.api.libs.json.{Format, JsResult, JsString, JsSuccess, JsValue, Json}
import service.OrderService.{AfternoonSlot, EveningSlot, MorningSlot, NightSlot, SlotTimings}

trait DeliveryService {

}

object DeliveryService {

  sealed trait Vehicle {
    def asString: String
    def vehicleCapacity: Double
    def vehicleCountAvailable: Int
    def availableSlots: List[SlotTimings]
  }

  case object Bike extends Vehicle {
    override def asString: String = "bike"

    override def vehicleCapacity: Double = 30.0

    override def vehicleCountAvailable: Int = 3

    override def availableSlots: List[SlotTimings] = List(MorningSlot, AfternoonSlot, EveningSlot)
  }

  case object Scooter extends Vehicle {
    override def asString: String = "scooter"

    override def vehicleCapacity: Double = 50

    override def vehicleCountAvailable: Int = 2

    override def availableSlots: List[SlotTimings] = List(MorningSlot, AfternoonSlot, EveningSlot)
  }

  case object Truck extends Vehicle {
    override def asString: String = "truck"

    override def vehicleCapacity: Double = 100

    override def vehicleCountAvailable: Int = 1

    override def availableSlots: List[SlotTimings] = List(AfternoonSlot, EveningSlot, NightSlot)
  }

  object Vehicle {

    def all = List(Bike, Scooter, Truck)

    implicit val formats = new Format[Vehicle] {
      override def reads(json: JsValue): JsResult[Vehicle] = json match {
        case JsString("bike") => JsSuccess(Bike)
        case JsString("scooter") => JsSuccess(Scooter)
        case JsString("truck") => JsSuccess(Truck)
        case _ => throw new Exception(s"Vehicle type $json not found.")
      }

      override def writes(o: Vehicle): JsValue = Json.toJson(o.asString)
    }
  }



  case class DeliveryPartner(partnerId: Long, vehicleType: Vehicle)
}