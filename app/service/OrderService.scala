package service

import com.google.inject.ImplementedBy
import play.api.libs.json._
import service.DeliveryService.Vehicle
import service.OrderService.{Order, SlotTimings, VehicleCarryingOrder}
import service.impl.OrderServiceImpl

import scala.collection.mutable.ListBuffer
import scala.concurrent.Future

@ImplementedBy(classOf[OrderServiceImpl])
trait OrderService{
  def assignOrderToVehicle(slot: SlotTimings, orders: List[Order]): Future[(List[VehicleCarryingOrder], List[Order])]
}

object OrderService {

  sealed trait SlotTimings {
    def asString: String
  }

  case object MorningSlot extends SlotTimings {
    override def asString: String = "6-9"
  }

  case object AfternoonSlot extends SlotTimings {
    override def asString: String = "9-13"
  }

  case object EveningSlot extends SlotTimings {
    override def asString: String = "16-19"
  }

  case object NightSlot extends SlotTimings {
    override def asString: String = "19-23"
  }

  case object UnknownSlot extends SlotTimings {
    override def asString: String = ""
  }

  object SlotTimings {
    implicit val formats = new Format[SlotTimings] {
      override def reads(json: JsValue): JsResult[SlotTimings] = json match {
        case JsString("6-9") => JsSuccess(MorningSlot)
        case JsString("9-13") => JsSuccess(AfternoonSlot)
        case JsString("16-19") => JsSuccess(EveningSlot)
        case JsString("19-23") => JsSuccess(NightSlot)
        case _  => JsSuccess(UnknownSlot)
      }

      override def writes(o: SlotTimings): JsValue = Json.toJson(o.asString)
    }
  }

  case class Order(orderId: Long, weight: Double, isAssigned: Boolean)

  case class VehicleCarryingOrder(vehicle: Vehicle, deliveryPartnerId: Long, orderIds: ListBuffer[Order], weight: Double)
}