package service.impl

import com.google.inject.{Inject, Singleton}
import models.{OrderRecord, OrderTable, VehicleTable}
import org.joda.time.DateTime
import play.api.Play
import play.api.db.slick.DatabaseConfigProvider
import service.DeliveryService.{Bike, Scooter, Truck, Vehicle}
import service.OrderService
import service.OrderService.{Order, SlotTimings, VehicleCarryingOrder}
import slick.backend.DatabaseConfig
import slick.driver.JdbcProfile
import scala.util.Random
import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class OrderServiceImpl @Inject()(val dbConfigProvider: DatabaseConfigProvider) extends OrderService {
  val dbConfig: DatabaseConfig[JdbcProfile] = dbConfigProvider.get[JdbcProfile]

  import dbConfig._

  override def assignOrderToVehicle(slot: SlotTimings, ordersToBeAssigned: List[Order]): Future[(List[VehicleCarryingOrder], List[Order])] = {
    for {
      vehicleList <- getAvailableVehicles(slot)
      _ = if (vehicleList.isEmpty) throw new Exception(s"No vehicle is available in the slot $slot")
      // The items are sorted in descending order
       orders = ordersToBeAssigned.sortBy(-_.weight)
       slotCapacity = Play.current.configuration.getDouble("slot_capacity").get
       totalOrdersWeight = orders.map(_.weight).sum
       _ = if (totalOrdersWeight > slotCapacity) {
         throw new Exception(s"Total Order capacity $totalOrdersWeight is more than slot capacity.")
       }

      vehicleListSortedAsc = vehicleList.sortBy(_.vehicleCapacity)
      vehicleListSortedDesc = vehicleList.sortBy(-_.vehicleCapacity)

      vehicleOpt = vehicleListSortedAsc.collectFirst { case vehicle if vehicle.vehicleCapacity >= totalOrdersWeight => vehicle }
      (completeOrders, incompleteOrders) = vehicleOpt match {
         case Some(vehicle) =>
           val orderListBuffer: ListBuffer[Order] = ListBuffer()
           orders.map(order => orderListBuffer += order.copy(isAssigned = true))
           (List(VehicleCarryingOrder(vehicle, 1, orderListBuffer, orderListBuffer.map(_.weight).sum)), List.empty)
         case None =>
           val countDesc = getIncompleteAndCompleteOrderStatus(vehicleListSortedDesc, totalOrdersWeight, orders)
           val countAsc = getIncompleteAndCompleteOrderStatus(vehicleListSortedAsc, totalOrdersWeight, orders)
           if (countDesc._2.nonEmpty == countAsc._2.nonEmpty) {
             if (countDesc._1.size > countAsc._1.size) countAsc else countDesc
           }
           else if (countDesc._2.nonEmpty < countAsc._2.nonEmpty) countDesc
           else countAsc
       }
      _ <- addOrderRecord(completeOrders.flatMap(_.orderIds))
    } yield {(completeOrders, incompleteOrders)}
  }

  private def getIncompleteAndCompleteOrderStatus(vehicles: List[Vehicle], weight: Double, pendingOrders: List[Order]) = {
    var totalOrdersWeight = weight
    var orders = pendingOrders
    val ordersDTO = vehicles.flatMap { vehicle =>
      var (vehicleCarryingOrders, incompleteOrders) = processTheOrders(totalOrdersWeight, vehicle, orders)
      orders = incompleteOrders
      totalOrdersWeight = totalOrdersWeight - vehicleCarryingOrders.map(_.weight).sum
      vehicleCarryingOrders
    }
    (ordersDTO, orders.collect { case o if !o.isAssigned => o })
  }

  private def addOrderRecord(orders: List[Order]) = {
    db.run( OrderTable.create(orders))
  }

  private def processTheOrders(totalOrdersWeight: Double, vehicle: Vehicle, order: List[Order]): (ListBuffer[VehicleCarryingOrder], List[Order]) = {
    var orders = order.sortBy(-_.weight)
    val vehiclesRequired = (totalOrdersWeight / vehicle.vehicleCapacity).ceil.toInt
    var vehiclesUsed = 0
    var weightLeft = totalOrdersWeight
    var vehicleBuffer: ListBuffer[VehicleCarryingOrder] = ListBuffer()

    while (vehiclesUsed < vehiclesRequired && vehiclesUsed < vehicle.vehicleCountAvailable) {
      var orderList: ListBuffer[Order] = ListBuffer()
      var vehicleCapacityLeft = vehicle.vehicleCapacity
      val updatedOrders = orders.map { order =>
        if (order.weight <= vehicleCapacityLeft && !order.isAssigned) {
          weightLeft = weightLeft - order.weight
          vehicleCapacityLeft = vehicleCapacityLeft - order.weight
          orderList += order.copy(isAssigned = true)
          order.copy(isAssigned = true)
        } else order
      }
      orders = updatedOrders
      vehiclesUsed = vehiclesUsed + 1
      val r = new scala.util.Random
      val partnerId = 1 + r.nextInt(( 20 - 1) + 1)
      vehicleBuffer += VehicleCarryingOrder(vehicle, partnerId, orderList, orderList.map(_.weight).sum)
    }
    val unCompletedOrders = orders.collect { case o if !o.isAssigned => o }
    (vehicleBuffer.collect { case dto if dto.orderIds.nonEmpty => dto }, unCompletedOrders)
  }

  private def getAvailableVehicles(slot: SlotTimings): Future[List[Vehicle]] = {
    val p = Vehicle.all.collect{case vehicle if vehicle.availableSlots.contains(slot) => vehicle}
    Future.successful(p)
//    Future.successful(List(Bike, Scooter, Truck))
//    db.run(VehicleTable.getAvailableVehicles(slot))
  }
}