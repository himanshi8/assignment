name := """grofer-assignment"""
organization := "com.example"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.12"

//
//libraryDependencies += "com.typesafe.slick" %% "slick" % "3.1.0"
//libraryDependencies += "org.postgresql" % "postgresql" % "9.4-1201-jdbc41"
//libraryDependencies += "com.typesafe.play" %% "play-slick-evolutions" % "2.0.0"
//libraryDependencies += "com.google.inject" % "guice" % "3.0"
//libraryDependencies += "com.typesafe.play" %% "play-json" % "2.5.14" withSources()
//libraryDependencies += "com.github.tminglei" %% "slick-pg" % "0.16.2"


libraryDependencies ++= List(
  "com.typesafe.play" %% "play-slick" % "2.1.0",
  "com.typesafe.play" %% "play-slick-evolutions" % "2.1.0",
  "com.typesafe.slick" %% "slick" % "3.2.0",
  "com.github.tototoshi" %% "slick-joda-mapper" % "2.3.0"
)
libraryDependencies += "com.github.tminglei" %% "slick-pg" % "0.16.2"

//libraryDependencies += jdbc
//libraryDependencies += evolutions

//libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.example.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.example.binders._"
